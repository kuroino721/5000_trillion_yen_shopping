chrome.extension.onMessage.addListener(function (request, sender, sendResponse) {
    if (request == "Action") {
        main();
    }
});

function main() {
    console.log("start!");
    let selectedText;
    let price;
    let flag = true;
    // マウスを離すと選択範囲の文字列が取得される
    document.onmouseup = function () {
        if (flag) {
            selectedText = window.getSelection().toString();
            console.log("selectedText: " + selectedText); // debug
            price = extractPrice(selectedText);
            console.log("price: " + price);// debug
            name = extractProductName(selectedText);
            console.log("name: " + name);// debug
            registerData(name, price);
            flag = false;
        }
    }
}

// 値段抽出
// 数字と単位（円, ￥）がセットになってる値を取り出す
function extractPrice(text) {
    console.log("extract price start!");
    if (text.match(/￥[\d,]*/g)) {
        return text.match(/￥[\d,]*/g)[0].replace(",", "").replace("￥", "");
    } else if (text.match(/\\[\d,]*/g)) {
        return text.match(/\\[\d,]*/g)[0].replace(",", "").replace("\\", "");
    } else if (text.match(/[\d,]*円/g)) {
        return text.match(/[\d,]*円/g)[0].replace(",", "").replace("円", "");
    }
}
// 商品名抽出
function extractProductName(text) {
    console.log("extract product name start!");
    // TODO: 商品名判定
    // chrome extension->[API]->Web App(NLP engine)
    return text;
}

// DBに格納
function registerData(name, price) {
    console.log("register data start!");
    // DBを開く
    var request = window.indexedDB.open("shoppingDB");

    // （そのverの）DBがない場合、onupgradeneededイベントが発生
    request.onupgradeneeded = function (event) {
        // DB作成
        var db = event.target.result;

        // objectStore作成。objectStoreの名前と、主キー(unique)を設定。
        var objectStore = db.createObjectStore("cart", { autoIncrement: "id" });

        // 属性を作成。属性の呼び名、属性名、unique属性を設定。
        objectStore.createIndex("name", "name", { unique: false });
        objectStore.createIndex("price", "price", { unique: false });
    }

    // DB作成完了 or 既存DBを開いた場合、onsuccessイベントが発生
    request.onsuccess = function (event) {
        db = event.target.result;
        //トランザクション開始
        var transaction = db.transaction("cart", "readwrite");
        // cartにnameとpriceを格納
        cart = transaction.objectStore("cart");
        cart.add({ name: name, price: price });
        console.log("data is registered!");
    }
}

// 拡張機能アイコン > 右クリック > カートの中身を見る、で別タブでページを開いてカートの中身を表示する


// カートの金額のトータルを出す




